import {getRepository} from "typeorm";
import {NextFunction, Request, Response,Router} from "express";
import { Controller } from "./Controller";
import { Comment } from "../entity/Comment";
import { Product } from "../entity/Product";
import logger from "../log/logger";
import { Sale } from "../entity/Sale";

export class SaleController extends Controller<Sale>{


    constructor(){
        super()
        this.addAllRouters(this.mainRouter)
    }

    async createEntityFromRequest(req: Request): Promise<Sale> {
        return await getRepository(Sale).create(req.body as Object)
    }
    async createCommentFromRequest(req: Request): Promise<Comment> {
        return await getRepository(Comment).create(req.body as Object)
    }

    async addGetRouter(router: any): Promise<void> {
        return null;
    }
    
    async addPostRouter(router: any): Promise<void> {
        await this.commentProduct(router)
        await this.buyProduct(router)
    }

     buyProduct = async(router: Router): Promise<void> => {
         router.post("/buy", async(req:Request, res:Response, next: NextFunction) => {
            logger.debug(`User request ${req.body}`)
            const stockRequested:number = Number(req.body.nb)
            const productToByu:Product = await getRepository(Product).findOne(Number(req.body.productId))
            let sale: Sale = await this.createEntityFromRequest(req)

            if(!productToByu){
                logger.warn(`user attempted to buy unaivalable product`)
                res.status(404).json({message: `No product with id ${req.body.productId}` })
            }
            if(stockRequested > productToByu.stock) {
                logger.warn(`user attempted to buy too much products`)
                res.status(400).json({message: `No sufficent stock with product $${req.body.productId}`})
            }

            await getRepository(Sale).save(sale)
                .then(async (saleDone) => {
                    if(saleDone) {
                        productToByu.stock -= stockRequested
                        await getRepository(Product).save(productToByu)

                        res.status(201).json({message: "Buy successfully"})
                    }
                })
         })
     }

     commentProduct = async (router: Router) => {
        router.post("/comment",async(req:Request, res:Response, next: NextFunction) => {
            const selectedProduct = await getRepository(Product) .findOne(Number(req.body.productId))

            if(!selectedProduct){
                res.status(404).json({error: "Missing user or product with theses ids"})
            } 

            const comment: Comment = await this.createCommentFromRequest(req)

            
            await getRepository(Comment).save(comment)
                .then(async (commentSaved) => {
                    if(commentSaved != undefined){
                        res.status(201).json({message: "Comment posted successfully"})
                    }
                })
                
        })
    }
   

    

    

}