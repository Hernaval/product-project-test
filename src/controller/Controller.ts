import {Router,Request} from "express"

export abstract class Controller<T>{
    mainRouter: Router = Router()

    protected addAllRouters = async(router: Router) : Promise<void> => {
        this.addGetRouter(router)
        this.addPostRouter(router)
    }

    abstract addGetRouter(router: Router) : Promise<void>
    abstract addPostRouter(router: Router) : Promise<void>
    abstract createEntityFromRequest(req: Request) : Promise<T>
}