import {Router} from "express"
import { ProductController } from "./ProductController"
import { SaleController } from "./SaleController"


const routerController: Router = Router()

routerController.use("/products",new ProductController().mainRouter)
routerController.use("/users",new SaleController().mainRouter)

export default routerController