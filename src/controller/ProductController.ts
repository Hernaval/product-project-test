import { Controller } from "./Controller";
import { Router, NextFunction, Request, Response } from "express";
import { Product } from "../entity/Product";
import { getRepository } from "typeorm";
import { Application } from "../Application";
import logger from "../log/logger";


export class ProductController extends Controller<Product>{
    
    constructor(){
        super()
        this.addAllRouters(this.mainRouter)
    }

    async createEntityFromRequest(req: any): Promise<Product> {
        return await getRepository(Product).create(req.body as Object)
    }

    async addGetRouter(router: Router): Promise<void> {
        await this.getProduct(router)
        await this.getSingleProduct(router)
    }

    getProduct =async (router: Router) : Promise<void>  => {
        router.get("",async(req:Request,res:Response, next: NextFunction) => {
            
           let allProducts: any[]
           
           const isExist = await new Promise(async (resolve,reject) => {
            Application.redisClient.get("product_list",(err, data)=> {
                if(err) reject(err)

                resolve(data != null)
            })
           })

           allProducts = await  new Promise(async (resolve, reject) => {
            if(!isExist){
                const data: Product[] = await getRepository(Product).find()
                Application.redisClient.set("product_list",JSON.stringify(data),(err) => {
                    if(err) reject(err)
                })
                resolve(data)
            }else{
                logger.info("get response from cache ")
                await Application.redisClient.get("product_list",async (err, data )=> {
                
                 resolve(await JSON.parse(data)) 

                })
            }
           })
           
          
           return res.status(200).json({data: allProducts})
        })
    }

    getSingleProduct = async(router: Router) : Promise<void> => {
        router.get("/:id",async(req:Request,res:Response, next: NextFunction) => {
            const id:number = Number(req.params.id)
            let product: Product = await getRepository(Product).findOne(id,{
                relations: ['comments']
            })
            /*if(!Application.redisClient.get(`product_${id}`)){
                product = await getRepository(Product).findOneOrFail(id)
                Application.redisClient.set(`product_${id}`,JSON.stringify(product))
            }else{
                logger.info("get response from cache ")
                product = Application.redisClient.get(`product_${id}`) as any
            } */

            return res.status(200).json({data: product})
        })
    }
    
    async addPostRouter(router: Router): Promise<void> {
        await this.saveProduct(router)
    }
    
    saveProduct = async(router: Router) : Promise<void> => {
        router.post("/",async(req:Request,res:Response, next: NextFunction) => {
           
                let productToSave: Product = await this.createEntityFromRequest(req)
                getRepository(Product).save(productToSave)
                    .then(productSaved => {
                        if(productSaved !== undefined) {
                            res.status(201).json({message: "Product saved successfuly"})
                        }
                    })
                    .catch(e => {
                        res.status(500).json({error: e.message})
                    })
        })
    }
}