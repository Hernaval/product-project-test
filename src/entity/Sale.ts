import {BeforeInsert, Column, Entity, ManyToOne} from "typeorm";
import { AbstractBaseEntity } from "./AbstractBaseEntity";
import { Product } from "./Product";;
import * as faker from "faker"

@Entity()
export class Sale extends AbstractBaseEntity {
    
    @ManyToOne(() => Product, product => product.sales)
    product: Product

    @Column()
    nb: number

    @Column()
    buyer: string

    @BeforeInsert()
    asignRandomNameToOwner = () => this.buyer = faker.name.firstName()
}
