import { Column, CreateDateColumn, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";
import { AbstractBaseEntity } from "./AbstractBaseEntity";
import { Comment } from "./Comment";
import { Sale } from "./Sale";
@Entity()
export class Product extends  AbstractBaseEntity{
   
    @Column()
    name: string

    @Column()
    stock : number

    @Column({unique: true})
    slugUrl : string

    @Column()
    description : string

    @Column()
    specification : string

    @Column()
    image : string

    @Column()
    price: number


    @OneToMany(() => Comment, comment => comment.product)
    @JoinTable()
    comments: Comment[]

    @OneToMany(() => Sale, sale => sale.product)
    sales: Sale[]

}
