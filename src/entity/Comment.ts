import {BeforeInsert, Column, Entity, ManyToOne} from "typeorm";
import { AbstractBaseEntity } from "./AbstractBaseEntity";
import { Product } from "./Product";
import * as faker from "faker"

@Entity()
export class Comment extends AbstractBaseEntity {
    
    @Column()
    message: string

    @ManyToOne(type => Product, product => product.comments)
    product: Product

    @Column()
    owner: string

    @BeforeInsert()
    asignRandomNameToOwner = () => this.owner = faker.name.firstName()

}
