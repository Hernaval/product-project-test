import * as winston from "winston"

 const logger = winston.createLogger({
    level: 'info,error,debug,warn',
    format: winston.format.json(),
})

export default logger