import { createConnection, getRepository } from "typeorm";
import { Product } from "./entity/Product";
import * as faker from "faker"
import { Comment } from "./entity/Comment";
import { Sale } from "./entity/Sale";

export const main = async () => {
  createConnection().then(async (c) => {
    const {lorem,image,random,name} = faker

    let count:number = 150;
    let pIds: number[] = []

    while(count > 0){
        let product: Product = await getRepository(Product).save({
            name: random.words(),
            stock: randomNumberBetweenRange(20),
            slugUrl: lorem.slug(),
            description: lorem.sentences(),
            specification: lorem.text(),
            image: image.imageUrl(),
            price: randomNumberBetweenRange(9999)
        });

        
        pIds.push(product.id)

        let userCommentProduct = await getRepository(Comment).save({
            owner: name.firstName(), 
            message: lorem.paragraph(),
            product: await getRepository(Product).findOneOrFail(pIds[randomNumberBetweenRange(pIds.length)]),
        })

        let userBuyProduct = await getRepository(Sale).save({
            buyer: name.firstName(),
            product: await getRepository(Product).findOneOrFail(pIds[randomNumberBetweenRange(pIds.length)]),
            nb: randomNumberBetweenRange(25)
        })

        count--
    }
    
  });
};

main()

export const randomNumberBetweenRange = (max:number): number => {
    return Math.floor(Math.random() * max)
}