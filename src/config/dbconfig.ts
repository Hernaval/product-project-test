import { ConnectionOptions } from "typeorm";

const devConnection: ConnectionOptions = {
   type: "postgres",
   host: "localhost",
   port: 9005,
   username: process.env.POSTGRES_USER || "admin",
   password: process.env.POSTGRES_PASSWORD || "admin",
   database: process.env.POSTGRES_DB || "product",
   synchronize: true,
   logging: true,
   entities: [
      "src/entity/**/*.ts"
   ],
   migrations: [
      "src/migration/**/*.ts"
   ],
   subscribers: [
      "src/subscriber/**/*.ts"
   ],
   cli: {
      entitiesDir: "src/entity",
      migrationsDir: "src/migration",
      subscribersDir: "src/subscriber"
   }
}

const prodConnection: ConnectionOptions = {
    type: "postgres",
    url: process.env.DATABASE_URL
}

export const ormConfig = process.env.DATABASE_URL ? prodConnection : devConnection