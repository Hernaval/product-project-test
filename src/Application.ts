var dotenv = require("dotenv")
import "reflect-metadata";
import * as cors from 'cors'
import * as express from "express";
import * as bodyParser from "body-parser";
import * as redis from "redis"
import { Connection, createConnection } from 'typeorm'
import router from "./routes";
import logger from "./log/logger";
import { ormConfig } from "./config/dbconfig";


export class Application{
    app: express.Application
    static redisClient: redis.RedisClient
    constructor(){
        this.app = express()
        dotenv.config()
        this.app.use(cors())
        this.app.use(bodyParser.json())
        this.app.use(bodyParser.urlencoded({extended : true}))
        this.app.use("/api",router)

        Application.redisClient = redis.createClient({
            port: Number(process.env.REDIS_PORT) || 6379,
            host:  process.env.REDIS_HOST || "localhost"
        })

    }

    setupDbAndServer= async() => {
        createConnection(ormConfig)
            .then(async (connection : Connection) => {
                await this.startServer()
            })
            .catch(e => {
                logger.error(`error creating connexion`)
            })
    }

     private startServer = async () : Promise<boolean> => {
        return await new Promise(async(resolve, reject) => {
            this.app.listen(process.env.PORT || 3001, () => {
                Application.redisClient.flushall("ASYNC",() => logger.info("flush redis db"))
                logger.info(`My server is running at port: ${process.env.PORT}`)

                //Application.redisClient.set("test","salut hernaval")
            })
        })
    }
}